import React from 'react';
import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom'

import Header from './components/Header';
import Productos from './components/Productos/Productos';
import Agregar from './components/Productos/Agregar';
import Editar from './components/Productos/Editar';

import { Provider } from 'react-redux';

import Store from "./store";

function App() {
  return (
    <Router>
      <Provider
        store={ Store }
      >
        <Header />
        <div className="container p-4">
          <Switch>
            <Route
              exact
              path="/"
              component={ Productos }
            />
            <Route
              exact
              path="/productos/agregar"
              component={ Agregar }
            />
            <Route
              exact
              path="/productos/editar/:id"
              component={ Editar }
            />
          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default App;
