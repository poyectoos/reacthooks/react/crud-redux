import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { editProduct } from '../../actions/producto';

import Spinner from '../Spinner';
import { showAlert, hideAlert } from '../../actions/alerta';

const Editar = () => {
  const history = useHistory();
  /*
  ==================================================
            Redux
  ==================================================
  */
 
  const dispatch = useDispatch();

  const selectedProduct = useSelector(state => state.products.product);
  const loading = useSelector(state => state.products.loading);
  const error = useSelector(state => state.products.error);
  const alerta = useSelector(state => state.alerta.alerta);
  /*
  ==================================================
            State
  ==================================================
  */

  const [ product, updateProduct ] = useState({
    id: null,
    nombre: '',
    precio: '',
  });
  
  useEffect(() => {
    dispatch(hideAlert());
    if (selectedProduct) {
      updateProduct({
        id: selectedProduct.id,
        nombre: selectedProduct.nombre,
        precio: selectedProduct.precio,
      });
    } else {
      history.push('/');
    }
    // eslint-disable-next-line
  }, [selectedProduct]);
  
  const { id, nombre, precio } = product;
  /*
  ==================================================
            Funciones
  ==================================================
  */
  const handleChangeInput = e => {
    updateProduct({
      ...product,
      [e.target.name]: e.target.value
    });
  }
  const handleSubmit = async e => {
    e.preventDefault();
    // Validamos formulario
    if ( isNaN(precio) || nombre.trim() === '' || Number(precio) <= 0) {
      const alerta = {
        msg: 'Todos los campos son obligatorios',
        type: 'danger'
      }
      dispatch(showAlert(alerta))
      return;
    }

    dispatch(hideAlert());

    dispatch(editProduct({
      id,
      nombre,
      precio: Number(precio),
    }));

    if (!error) {
      history.push('/');
    }
  }


  return (
    <div className="row justify-content-center">
      <div className="col-md-8">
        <div className="card">
          <div className="card-body">
            <h2 className="text-center mb-2 font-weight-bold">
              Editar producto
            </h2>
            {
              alerta
                ?
              <div className={`alert alert-${alerta.type}`}>
                { alerta.msg }
              </div>
                :
              null
            }
            <form
              onSubmit={ handleSubmit }
            >
              <div className="form-group">
                <label
                  htmlFor="nombre"
                >
                  Nombre del producto
                </label>
                <input
                  type="text"
                  name="nombre"
                  id="nombre"
                  value={ nombre }
                  onChange={ handleChangeInput }
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label
                  htmlFor="precio"
                >
                  Precio
                </label>
                <input
                  type="number"
                  name="precio"
                  id="precio"
                  value={ precio }
                  onChange={ handleChangeInput }
                  className="form-control"
                />
              </div>
              <div className="form-group">
                {
                  loading
                    ?
                  <Spinner />
                    :
                  <button
                    type="submit"
                    className="btn btn-primary font-weight-bold d-block w-100"
                  >
                    Agregar
                  </button>
                }
              </div>
              {
                error
                  ?
                <div className="alert alert-danger">
                  Error al guardar producto
                </div>
                  :
                null
              }
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Editar;