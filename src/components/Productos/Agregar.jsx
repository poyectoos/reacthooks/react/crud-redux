import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector  } from 'react-redux';

import { addProduct } from '../../actions/producto';
import { showAlert, hideAlert } from '../../actions/alerta';

import Spinner from '../Spinner';

const Agregar = ({ history }) => {

  /*
  ==================================================
            Redux
  ==================================================
  */
  // Propiedad que nos permite tomar los actions
  const dispatch = useDispatch();

  const loading = useSelector(state => state.products.loading);
  const error = useSelector(state => state.products.error);
  const alerta = useSelector(state => state.alerta.alerta);
  
  console.log(error);
  /*
  ==================================================
            State local
  ==================================================
  */
  const [ product, updateProduct ] = useState({
    nombre: '',
    precio: '',
  });

  const { nombre, precio } = product;

  useEffect(() => {
    dispatch(hideAlert());
    // eslint-disable-next-line
  }, []);
  /*
  ==================================================
            Funciones del componente
  ==================================================
  */
  const handleChangeInput = e => {
    updateProduct({
      ...product,
      [e.target.name]: e.target.value
    });
  }
  

  const handleSubmit = async e => {
    e.preventDefault();
    // Validamos formulario
    if ( isNaN(precio) || nombre.trim() === '' || Number(precio) <= 0) {
      const alerta = {
        msg: 'Todos los campos son obligatorios',
        type: 'danger'
      }
      dispatch(showAlert(alerta));
      return;
    }

    dispatch(hideAlert());

    dispatch(addProduct({
      nombre,
      precio: Number(precio) 
    }));

    if (!error) {
      history.push('/');
    }
  }

  return (
    <div className="row justify-content-center">
      <div className="col-md-8">
        <div className="card">
          <div className="card-body">
            <h2 className="text-center mb-2 font-weight-bold">
              Agregr nuevo producto
            </h2>
            {
              alerta
                ?
              <div className={`alert alert-${alerta.type}`}>
                { alerta.msg }
              </div>
                :
              null
            }
            <form
              onSubmit={ handleSubmit }
            >
              <div className="form-group">
                <label
                  htmlFor="nombre"
                >
                  Nombre del producto
                </label>
                <input
                  type="text"
                  name="nombre"
                  id="nombre"
                  value={ nombre }
                  onChange={ handleChangeInput }
                  className="form-control"
                  autoComplete="off"
                />
              </div>
              <div className="form-group">
                <label
                  htmlFor="precio"
                >
                  Precio
                </label>
                <input
                  type="number"
                  name="precio"
                  id="precio"
                  value={ precio }
                  onChange={ handleChangeInput }
                  className="form-control"
                  autoComplete="off"
                  pattern="[0-9]"
                />
              </div>
              <div className="form-group">
                {
                  loading
                    ?
                  <Spinner />
                    :
                  <button
                    type="submit"
                    className="btn btn-primary font-weight-bold d-block w-100"
                  >
                    Agregar
                  </button>
                }
              </div>
              {
                error
                  ?
                <div className="alert alert-danger">
                  Error al guardar producto
                </div>
                  :
                null
              }
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Agregar;