import React, { Fragment, useEffect } from 'react';
import { useDispatch ,useSelector } from 'react-redux';

import { getProducts } from '../../actions/producto';

import Producto from './Producto';
import Spinner from '../Spinner';

const Productos = () => {
  /*
  ==================================================
            Redux
  ==================================================
  */
  const dispatch = useDispatch();
  const loading = useSelector(state => state.products.loading);
  const products = useSelector(state => state.products.products);
  const error = useSelector(state => state.products.error);
  /*
  ==================================================
            Hooks
  ==================================================
  */
  useEffect(() => {
    const handleGetProjects = () => dispatch(getProducts());
    handleGetProjects();
    // eslint-disable-next-line
  }, []);

  const MostrarError = () => {
    if (error) {
      return (
        <div className="alert alert-danger">
          Error al cargar la lista de productos
        </div>
      );
    } else {
      return null;
    }
  }
  
  return (
    <Fragment>
      <h2 className="text-center my5">Listado de productos</h2>
      <MostrarError />
      {
        loading
          ?
        <Spinner />
          :
        <table className="table table-striped align-middle">
          <thead className="bg-primary table-dark">
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Precio</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody className="table-hover">
            {
              products.length === 0
                ?
              <tr>
                <td colSpan="3">No hay productos</td>
              </tr>
                :
              products.map(
                product => (
                  <Producto
                    key={ product.id }
                    product={ product }
                  />
                )
              )
            }
          </tbody>
        </table>
      }
    </Fragment>
  );
};

export default Productos;