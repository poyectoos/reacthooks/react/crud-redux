import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import Swal from 'sweetalert2';

import { useDispatch } from 'react-redux';
import { deleteProduct, selectProduct } from '../../actions/producto';

const Producto = ({ product }) => {
  // Dispatch para las funciones
  const dispatch = useDispatch();
  // History para redirecciones
  const history = useHistory();

  const { id, nombre, precio } = product;

  const handleDelete = async () => {
    const confirmacion = await Swal.fire({
      title: '¿Está seguro de eliminar el producto?',
      text: `${nombre} no aparecera en la lista de productos nunca mas`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#ff7851',
      cancelButtonColor: '#78c2ad',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
    })
    if (confirmacion.isConfirmed) {
      dispatch(deleteProduct(id));
    }
  }

  const handleEdit = () => {
    dispatch(selectProduct({id, nombre, precio}));
    history.push(`/productos/editar/${id}`)
  }


  return (
    <tr>
      <th scope="row">
        { nombre }
      </th>
      <td>
        { precio }
      </td>
      <td className="acciones">
        <button
          type="button"
          className="btn btn-primary mx-1"
          onClick={ handleEdit }
        >
          Editar
        </button>
        <button
          type="button"
          className="btn btn-danger mx-1"
          onClick={ handleDelete }
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
};

Producto.propTypes = {
  product: PropTypes.object.isRequired
};

export default Producto;