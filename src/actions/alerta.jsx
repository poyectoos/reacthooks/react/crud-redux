import { SHOW_ALERT, HIDE_ALERT } from '../types/alerta';

/*
==================================================
          Action que muestra la alerta y su mensaje
==================================================
*/
export const showAlert = (alerta) => {
  return (dispatch) => {
    dispatch( onShowAlert(alerta) );
  }
}
const onShowAlert = (alerta) => {
  return ({
    type: SHOW_ALERT,
    payload: alerta
  });
}
/*
==================================================
          Action que oculta la alerta
==================================================
*/
export const hideAlert = () => {
  return (dispatch) => {
    dispatch( onHideAlert() );
  }
}
const onHideAlert = () => {
  return ({
    type: HIDE_ALERT,
  });
}
