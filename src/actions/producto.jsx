import Swal from 'sweetalert2';
import Client from '../axios/client';

import clientAxios from '../axios/client';

import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_ERROR,

  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,

  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
  
  SELECT_PRODUCT,
  EDIT_PRODUCT,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_ERROR,
} from '../types/productos';
/*
==================================================
          Este acton se encarga de agregar productos
==================================================
*/
export const addProduct = ({ nombre, precio }) => {
  return async (dispatch) => {
    dispatch({
      type: ADD_PRODUCT,
    });
    try {
      // Se comunica con la DB
      await clientAxios.post('productoss', {
        nombre,
        precio,
      });
      dispatch({
        type: ADD_PRODUCT_SUCCESS,
        payload: { nombre, precio }
      });
      alerta('success', 'Produto agregado correctamente');
    } catch (error) {
      dispatch({
        type: ADD_PRODUCT_ERROR
      });
    }
  }
}
/*
==================================================
          Action para obtener la lista de productos
==================================================
*/
export const getProducts = () => {
  return async (dispatch) => {
    dispatch({
      type: GET_PRODUCT,
    });
    try {
      const answ = await Client.get('productos');

      dispatch({
        type: GET_PRODUCT_SUCCESS,
        payload: answ.data,
      });

    } catch (error) {
      dispatch({
        type: GET_PRODUCT_ERROR,
      });
    }
  }
}
/*
==================================================
          Action para eliminar un producto
==================================================
*/
export const deleteProduct = id => {
  return async (dispatch) => {
    // Se prepara el elemento a eliminar
    dispatch({
      type: DELETE_PRODUCT,
      payload: id,
    })
    try {
      await Client.delete(`productos/${id}`);
      dispatch({
        type: DELETE_PRODUCT_SUCCESS,
      })
      alerta('success', 'Produto eliminado correctamente');
    } catch (error) {
      console.log(error);
      dispatch({
        type: DELETE_PRODUCT_ERROR,
      })
    }
  }
}
/*
==================================================
          Seleccionamos producto a editas
==================================================
*/
export const selectProduct = (product) => {
  const { id, nombre, precio } = product;
  return (dispatch) => {
    dispatch({
      type: SELECT_PRODUCT,
      payload: { id, nombre, precio }
    })
  }
}

export const editProduct = product => {
  const { id, nombre, precio } = product;
  return async (dispatch) => {
    dispatch({
      type: EDIT_PRODUCT,
    })
    try {
      const answ = await Client.put(`productoss/${id}`, { nombre, precio });
      console.log(answ);

      dispatch({
        type: EDIT_PRODUCT_SUCCESS,
        payload: product,
      })

      alerta('success','Producto editado correctamente');
    } catch (error) {
      dispatch({
        type: EDIT_PRODUCT_ERROR,
      })
    }
  }
}

/*
==================================================
          Helpers
==================================================
*/

const alerta = (type, msg) => {
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
  })
  
  Toast.fire({
    icon: type,
    title: msg
  })
}