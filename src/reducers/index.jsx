import { combineReducers } from 'redux';
import productos from './productos';
import alerta from './alerta';

export default combineReducers({
  products: productos,
  alerta,
});