import { SHOW_ALERT, HIDE_ALERT } from '../types/alerta';

const initialState = {
  alerta: null
}

const Alerta = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_ALERT:
      return {
        ...state,
        alerta: action.payload
      }
    case HIDE_ALERT:
      return {
        ...state,
        alerta: null
      }
    default:
      return state;
  }
}

export default Alerta;