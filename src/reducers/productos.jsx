

import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_ERROR,

  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,
  
  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,

  SELECT_PRODUCT,
  EDIT_PRODUCT,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_ERROR
} from '../types/productos';

// Cada reducer tiene su propio state

const initialState = {
  products: [],
  error: null,
  loading: false,

  product: null
}

const Reducer = (state = initialState, action) => {
  switch (action.type) {
    // Se manda la cunsulta a la base de datos para mostrar loader
    case EDIT_PRODUCT:
    case GET_PRODUCT:
    case ADD_PRODUCT:
      return {
        ...state,
        loading: true,
      }
    case ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        products: [
          ...state.products,
          action.payload
        ],
        error: null,
      }
    case GET_PRODUCT_ERROR:
    case ADD_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      }
    case GET_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        products: action.payload,
      }
    case DELETE_PRODUCT:
      return {
        ...state,
        product: action.payload,
      }
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        products: state.products.filter(
          product => (
            product.id !== state.product
          )
        ),
        product: null,
      }
    case DELETE_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        product: null,
      }
    case SELECT_PRODUCT: {
      return {
        ...state,
        product: action.payload,
      }
    }
    case EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        products: state.products.map(
          product => 
          (
            product.id === action.payload.id
              ?
            action.payload
              :
            product
          )
        ),
        product: null,
        loading: false,
        error: null,
      }
    case EDIT_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      }
    default:
      return state;
  }
}

export default Reducer;